<?php


class mysql {
    public static $ins = NULL;
    public $conn = NULL;
    public $conf = array();
    
 public function __construct() {
        $this->connect('localhost','root','1234');
        $this->select_db('test');
        $this->setChar('utf8');
    }


    public function __destruct() {
    }

    public static function getIns() {
        if(!(self::$ins instanceof self)) {
            self::$ins = new self();
        }

        return self::$ins;
    }

    public function connect($h,$u,$p) {
        $this->conn = mysql_connect($h,$u,$p);
        if(!$this->conn) {
            $err = new Exception('连接失败');
            throw $err;
        }
    }

    protected function select_db($db) {
        $sql = 'use ' . $db;
        $this->query($sql);
    }

    protected function setChar($char) {
        $sql = 'set names ' . $char;
        return $this->query($sql);
    }

    public function query($sql) {

        $rs = mysql_query($sql,$this->conn);

       log::write($sql);

        return $rs;
    }

    public function autoExecute($table,$arr,$mode='insert',$where = ' where 1 limit 1') {
        /*    insert into tbname (username,passwd,email) values ('',)
        /// 把所有的键名用','接起来
        // implode(',',array_keys($arr));
        // implode("','",array_values($arr));
        */
        
        if(!is_array($arr)) {
            return false;
        }

        if($mode == 'update') {
            $sql = 'update ' . $table .' set ';
            foreach($arr as $k=>$v) {
                $sql .= $k . "='" . $v ."',";
            }
            $sql = rtrim($sql,',');
            $sql .= $where;
            return $this->query($sql);
        }

        $sql = 'insert into ' . $table . ' (' . implode(',',array_keys($arr)) . ')';
        $sql .= ' values (\'';
        $sql .= implode("','",array_values($arr));
        $sql .= '\')';

        return $this->query($sql);
    
    }

    public function getAll($sql) {
        $rs = $this->query($sql);
        
        $list = array();
        while($row = mysql_fetch_assoc($rs)) {
            $list[] = $row;
        }

        return $list;
    }

    public function getRow($sql) {
        $rs = $this->query($sql);
        
        return mysql_fetch_assoc($rs);
    }

    public function getOne($sql) {
        $rs = $this->query($sql);
        $row = mysql_fetch_row($rs);

        return $row[0];
    }

    // 返回影响行数的函数
    public function affected_rows() {
        return mysql_affected_rows($this->conn);
    }

    // 返回最新的auto_increment列的自增长的值
    public function insert_id() {
        return mysql_insert_id($this->conn);
    }


}

class Log {

    const LOGFILE = 'curr.log'; //建一个常量,代表日志文件的名称

    // 写日志的
    public static function write($cont) {
        $cont .= "\r\n";
        // 判断是否备份
        $log = self::isBak(); // 计算出日志文件的地址
        
        $fh = fopen($log,'ab');
        fwrite($fh,$cont);
        fclose($fh); 
    }

    // 备份日志
    public static function bak() {
        // 就是把原来的日志文件,改个名,存储起来
        // 改成 年-月-日.bak这种形式

        $log = self::LOGFILE;
        $bak = date('ymd') . mt_rand(10000,99999) . '.bak';
        return rename($log,$bak);
    }

    // 读取并判断日志的大小
    public static function isBak() {
        $log =  self::LOGFILE;
        
        if(!file_exists($log)) { //如果文件不存在,则创建该文件
            touch($log);    // touch在linux也有此命令,是快速的建立一个文件
            return $log;
        }

        // 要是存在,则判断大小
        // 清除缓存
        clearstatcache(true,$log);
        $size = filesize($log);
        if($size <= 1024 * 1024) { //大于1M
            return $log;
        }
        
        // 走到这一行,说明>1M
        if(!self::Bak()) {
            return $log;
        } else {
            touch($log);
            return $log;
        }
    }
}