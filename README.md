local-info
=======
## 在前面
因为平常注册各种论坛的帐户太多了，所以搞了这么一个小东西来记录而已。<br/>
界面就一个BP表格，然后下拉AJAX请求显示。<br/>
一个简单信息记录程序，<b>仅限本地使用，请勿在互联网上使用</b>。<br/><br/>

## 使用方法：<br/>
首先导入SQL文件test.sql，<br/>
然后修改文件mysql.class.php的__construct方法，配置你的数据库信息，<br/>
运行index.php文件。<br/><br/>

## 联系方式
有问题请留言我的<a href="http://blog.clmao.com/?page_id=145" target="_blank">博客</a>